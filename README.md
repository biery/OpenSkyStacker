# OpenSkyStacker

Multi-platform astroimaging stacker built in Qt and leveraging OpenCV.

## Setup

Qt and OpenCV are required for this project. Instructions on how to install OpenCV with Qt on Linux and Windows can be found [here](https://wiki.qt.io/OpenCV_with_Qt).